<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Casting
 */
Route::resource('casting', 'Casting\CastingController', ['only' => ['index','show']]);
Route::resource('casting.movie', 'Casting\CastingMovieController', ['only' => ['index']]);

/**
 * Producer
 */
Route::resource('producers', 'Producer\ProducerController', ['only' => ['index','show']]);
Route::resource('producers.movie', 'Producer\ProducerMovieController', ['only' => ['index']]);

/**
 * Director
 */
Route::resource('directors', 'Director\DirectorController', ['only' => ['index','show']]);
Route::resource('directors.movie', 'Director\DirectorMovieController', ['only' => ['index']]);

/**
 * Movies
 */
Route::resource('movies', 'Movie\MovieController', ['except' => ['create','edit']]);
Route::resource('movies.casting', 'Movie\MovieCastingController', ['only' => ['index']]);
Route::resource('movies.directors', 'Movie\MovieDirectorController', ['only' => ['index']]);
Route::resource('movies.producers', 'Movie\MovieProducerController', ['only' => ['index']]);

/**
 * Users
 */
Route::resource('users', 'User\UserController', ['except' => ['create','edit']]);