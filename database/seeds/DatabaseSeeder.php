<?php

use App\User;
use App\Movie;
use App\Image;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Services\Image\ImageService;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        Image::truncate();
        User::truncate();
        Movie::truncate();
        DB::table('director_movie')->truncate();
        DB::table('movie_producer')->truncate();
        DB::table('casting_movie')->truncate();

        $usersNumber = 30;
        $moviesNumber = 15;

        // first images seeder
        $imageService = app(ImageService::class);
        $imageService->imagesSeeder();

        // then apply User and Movie seeders
        factory(User::class, $usersNumber)->create();
        factory(Movie::class, $moviesNumber)->create()->each(
            function ($movie) {
                $casting = User::all()->random(mt_rand(3, 10))->pluck('id');
                $movie->casting()->attach($casting);

                $directors = User::all()->random(mt_rand(1, 3))->pluck('id');
                $movie->directors()->attach($directors);

                $producers = User::all()->random(mt_rand(1, 3))->pluck('id');
                $movie->producers()->attach($producers);
            }
        );
    }
}
