<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Movie;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(6),
        'release_year' => $faker->year(),
        'synopsis' => $faker->paragraph(3),
        'image_id' => $faker->randomElement([3,4,5,6,7,8,9,10,11,12,13,14,15])
    ];
});
