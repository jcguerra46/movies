<?php

namespace App\Services\Movie;

use App\Movie;
use App\Traits\ApiResponser;
use App\Services\Image\ImageService;
use Symfony\Component\HttpFoundation\Response;

class MovieService
{
    use ApiResponser;

    /**
     * Store a newly created resource in storage.
     *
     * @param $request
     * @return mixed
     */
    public function createMovie($request)
    {
        $input = $request->all();
        if($request->image_id){
            $imageService = app(ImageService::class);
            $input['image_id'] = $imageService->setImage($request->image_id);
        }

        $movie = Movie::create($input);
        $this->attachDirectors($movie, $request->directors);
        $this->attachProducers($movie, $request->producers);
        $this->attachCasting($movie, $request->casting);
        return $movie;
    }

    public function attachDirectors($movie, $directors)
    {
        foreach($directors as $director)
        {
            $movie->directors()->attach($director['id']);
        }
    }

    public function attachProducers($movie, $producers)
    {
        foreach($producers as $producer)
        {
            $movie->producers()->attach($producer['id']);
        }
    }

    public function attachCasting($movie, $casting)
    {
        foreach($casting as $cast)
        {
            $movie->casting()->attach($cast['id']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $request
     * @param $movie_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMovie($request, $movie_id)
    {
        $data = $request->all();
        $movie = Movie::findOrFail($movie_id);

        if($request->has('title')) {
            $movie->title = $request->title;
        }

        if($request->has('release_year')) {
            $movie->release_year = $request->release_year;
        }

        if($request->has('synopsis')) {
            $movie->synopsis = $request->synopsis;
        }

        if($request->has('image_id')) {
            $imageService = app(ImageService::class);
            $movie->image_id = $imageService->setImage($request->file('image_id'));
        }

        $movie->save();

        if($request->has('directors')){
            $this->syncDirectors($movie, $data['directors']);
        }
        if($request->has('producers')){
            $this->syncProducers($movie, $data['producers']);
        }
        if($request->has('casting')){
            $this->syncCasting($movie, $data['casting']);
        }
        return $movie;
    }

    public function syncDirectors($movie, $directors)
    {
        foreach($directors as $director)
        {
            $movie->directors()->sync($director['id']);
        }
    }

    public function syncProducers($movie, $producers)
    {
        foreach($producers as $producer)
        {
            $movie->producers()->sync($producer['id']);
        }
    }

    public function syncCasting($movie, $casting)
    {
        foreach($casting as $cast)
        {
            $movie->casting()->sync($cast['id']);
        }
    }

    public function destroyMovie($movie)
    {
        $movie->directors()->detach();
        $movie->producers()->detach();
        $movie->casting()->detach();
        $movie->delete();
        return $movie;
    }
}