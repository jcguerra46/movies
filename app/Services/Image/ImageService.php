<?php

namespace App\Services\Image;

use App\Image;

class ImageService
{
    /**
     * Store a newly created resource in storage.
     *
     * @param $file
     * @return mixed
     */
    public function setImage($file)
    {
        $name = time() . '_' . $file->getClientOriginalName();
        $file->move('images', $name);
        $image = Image::create(['file' => $name]);
        return $image->id;
    }

    /**
     * Create data for images seeder
     */
    public static function imagesSeeder()
    {
        $images = [
            'female.png',
            'male.jpg',
            'aladdin.jpg',
            'alita.jpg',
            'annabelle.jpg',
            'avengers.jpg',
            'captain_marvel.jpg',
            'childs_play.jpg',
            'godzilla.jpg',
            'hellboy.jpg',
            'john_wick.jpg',
            'MIB.jpg',
            'spiderman.jpg',
            'toy_story.jpg',
            'x_men.jpg'
        ];
        foreach($images as $image){
            Image::create(['file' => $image]);
        }
    }
}