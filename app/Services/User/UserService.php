<?php

namespace App\Services\User;

use App\User;
use App\Image;
use App\Traits\ApiResponser;
use App\Services\Image\ImageService;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use Symfony\Component\HttpFoundation\Response;

class UserService
{
    use ApiResponser;

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreUserRequest $request
     * @return mixed
     */
    public function storeUser(StoreUserRequest $request)
    {
        $data = $request->all();
        $data['verified'] = User::USER_NOT_VERIFIED;
        $data['verification_token'] = User::generateTokenVerification(40);
        $data['admin'] = User::REGULAR_USER;

        // if has image
        if($file = $request->file('image_id')){
            $imageService = app(ImageService::class);
            $image = $imageService->setImage($file);
            $data['image_id'] = $image->id;
        }
        $user = User::create($data);
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUserRequest $request
     * @param User $user
     * @return User
     */
    public function updateUser(UpdateUserRequest $request, User $user)
    {
        if ($request->has('first_name')) {
            $user->first_name = $request->first_name;
        }

        if ($request->has('last_name')) {
            $user->last_name = $request->last_name;
        }

        if ($request->has('aliases')) {
            $user->aliases = $request->aliases;
        }

        if ($request->has('email') && $user->email != $request->email) {
            $user->verified = User::USER_NOT_VERIFIED;
            $user->verification_token = User::generateTokenVerification(40);
            $user->email = $request->email;
        }

        if ($request->has('password')) {
            $user->password = $request->password;
        }

        if ($request->has('admin')) {
            if (!$user->isVerified()) {
                return $this->errorResponse('Only verified users can modify this value',  Response::HTTP_CONFLICT);
            }
            $user->admin = $request->admin;
        }

        if($request->has('biography')) {
            $user->biography = $request->biography;
        }

        if($request->has('image_id')) {
            $imageService = app(ImageService::class);
            $image = $imageService->setImage($request->file('image_id'));
            $data['image_id'] = $image->id;
        }

        if (!$user->isDirty()) {
            return $this->errorResponse('You must specify at least one different value to update', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user->save();
        return $user;
    }
}