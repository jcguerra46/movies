<?php

namespace App\Http\Controllers\Movie;

use App\Movie;
use Illuminate\Http\Request;
use App\Services\Movie\MovieService;
use App\Http\Controllers\ApiController;
use App\Http\Requests\Movie\StoreMovieRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Movie\UpdateMovieRequest;

class MovieController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * Swagger Annotations
     * * @OA\Get(
     *     path="/movies",
     *     tags={"Movies"},
     *     summary="Get movies list",
     *     description="Returns list of movies.",
     *     operationId="index",
     *     @OA\Response(
     *         response=200,
     *         description="Movies overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     *
     */
    public function index()
    {
        $movies = Movie::all();
        return $this->showAll($movies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    /**
     * Swagger Annotation
     * @OA\Post(
     *     path="/movies",
     *     tags={"Movies"},
     *     summary="Create movie",
     *     description="Create movie.",
     *     operationId="store",
     *     @OA\Parameter(
     *         name="title",
     *         in="query",
     *         description="Title",
     *         required=true,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="release_year",
     *         in="query",
     *         description="Release Year",
     *         required=true,
     *         @OA\Schema(
     *             type="number"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="synopsys",
     *         in="query",
     *         description="Synopsis",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             format=""
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="image_id",
     *         in="query",
     *         description="Image file",
     *         required=false,
     *         @OA\Schema(
     *             type="file"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Users overview.",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description=" Unprocessable Entity.",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     *
     */
    public function store(StoreMovieRequest $request)
    {
        $movieService = app(MovieService::class);
        $movie = $movieService->createMovie($request);
        return $this->showOne($movie, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Movie $movie
     * @return \Illuminate\Http\JsonResponse
     */
    /**
     * Swagger Annotations
     * @OA\Get(
     *     path="/movies/{id}",
     *     tags={"Movies"},
     *     summary="Get a movie info",
     *     description="Returns a movie info.",
     *     operationId="show",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Movie ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Movie overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Movie not found.",
     *     )
     * )
     *
     */
    public function show(Movie $movie)
    {
        return $this->showOne($movie);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Movie $movie
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    /**
     * Swagger Annotation
     * @OA\Put(
     *     path="/movies/{id}",
     *     tags={"Movies"},
     *     summary="Update movie",
     *     description="Update movie.",
     *     operationId="update",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Movie id",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="title",
     *         in="query",
     *         description="Title",
     *         required=false,
     *         @OA\Schema(
     *             type="string"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="release_year",
     *         in="query",
     *         description="Release Year",
     *         required=false,
     *         @OA\Schema(
     *             type="number"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="synopsys",
     *         in="query",
     *         description="Synopsis",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             format=""
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="image_id",
     *         in="query",
     *         description="Image file",
     *         required=false,
     *         @OA\Schema(
     *             type="file"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Users overview."
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description=" Unprocessable Entity.",
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     *
     */
    public function update(UpdateMovieRequest $request, $movie_id)
    {
        $movieService = app(MovieService::class);
        $movie = $movieService->updateMovie($request, $movie_id);
        return $this->showOne($movie);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Movie $movie
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    /**
     * Swagger Annotation
     * @OA\Delete(
     *     path="/movies/{id}",
     *     tags={"Movies"},
     *     summary="Delete movie",
     *     description="Delete movie.",
     *     operationId="destroy",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Movie ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Movie deleted."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Movie not found.",
     *     )
     * )
     *
     */
    public function destroy(Movie $movie)
    {
        $movieService = app(MovieService::class);
        $movie = $movieService->destroyMovie($movie);
        return $this->showOne($movie);
    }
}
