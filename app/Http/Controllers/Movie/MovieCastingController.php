<?php

namespace App\Http\Controllers\Movie;

use App\Movie;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class MovieCastingController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Swagger Annotations
     * * @OA\Get(
     *     path="/movies/{id}/casting",
     *     tags={"Movies"},
     *     summary="Get casting list of the movie",
     *     description="Returns casting list of the movie.",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Movie ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Movies overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     *
     */
    public function index(Movie $movie)
    {
        $casting = $movie->casting;
        return $this->showAll($casting);
    }
}
