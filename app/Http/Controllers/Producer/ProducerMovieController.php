<?php

namespace App\Http\Controllers\Producer;

use App\Producer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ProducerMovieController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Swagger Annotations
     * * @OA\Get(
     *     path="/producers/{id}/movie",
     *     tags={"Producers"},
     *     summary="Get movies of the Producer",
     *     description="Returns movies of the producer.",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Producer ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Movies overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     *
     */
    public function index(Producer $producer)
    {
        $movies = $producer->movies;
        return $this->showAll($movies);
    }
}
