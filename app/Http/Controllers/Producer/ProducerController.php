<?php

namespace App\Http\Controllers\Producer;

use App\User;
use App\Producer;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class ProducerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Swagger Annotations
     * * @OA\Get(
     *     path="/producers",
     *     tags={"Producers"},
     *     summary="Get producers list",
     *     description="Returns producers list.",
     *     operationId="index",
     *     @OA\Response(
     *         response=200,
     *         description="Movies overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     *
     */
    public function index()
    {
        $producers = Producer::has('movies')->get();
        return $this->showAll($producers);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Swagger Annotation
     * @OA\Get(
     *     path="/producers/{id}",
     *     tags={"Producers"},
     *     summary="Get a producers info",
     *     description="Returns a producers info.",
     *     operationId="show",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Producers ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Producers overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Producers not found.",
     *     )
     * )
     *
     */
    public function show(Producer $producer)
    {
        return $this->showOne($producer);
    }
}
