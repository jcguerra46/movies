<?php

namespace App\Http\Controllers\Director;

use App\Director;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class DirectorMovieController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Swagger Annotations
     * * @OA\Get(
     *     path="/directors/{id}/movie",
     *     tags={"Directors"},
     *     summary="Get movies of the director",
     *     description="Returns movies of the director.",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Director ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Movies overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     *
     */
    public function index(Director $director)
    {
        $movies = $director->movies;
        return $this->showAll($movies);
    }
}
