<?php

namespace App\Http\Controllers\Director;

use App\Director;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class DirectorController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Swagger Annotations
     * * @OA\Get(
     *     path="/directors",
     *     tags={"Directors"},
     *     summary="Get directors list",
     *     description="Returns directors list.",
     *     operationId="index",
     *     @OA\Response(
     *         response=200,
     *         description="Movies overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     *
     */
    public function index()
    {
        $directors = Director::has('movies')->get();
        return $this->showAll($directors);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Swagger Annotation
     * @OA\Get(
     *     path="/directors/{id}",
     *     tags={"Directors"},
     *     summary="Get a directors info",
     *     description="Returns a directors info.",
     *     operationId="show",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Director ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Director overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Director not found.",
     *     )
     * )
     *
     */
    public function show(Director $director)
    {
        return $this->showOne($director);
    }
}
