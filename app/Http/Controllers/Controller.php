<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @SWG\Swagger(
     *     @OA\Info (
     *          title = "Movies",
     *          version = "0.1",
     *          @OA\Contact(
     *              email="juancarlosguerra46@gmail.com"
     *          ),
     *     )
     * )
     */
}
