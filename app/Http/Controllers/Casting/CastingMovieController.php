<?php

namespace App\Http\Controllers\Casting;

use App\Casting;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CastingMovieController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Swagger Annotations
     * * @OA\Get(
     *     path="/casting/{id}/movie",
     *     tags={"Casting"},
     *     summary="Get movie list of the casting",
     *     operationId="index",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Casting ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Casting overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     )
     * )
     *
     */
    public function index(Casting $casting)
    {
        $movies = $casting->movies;
        return $this->showAll($movies);
    }
}
