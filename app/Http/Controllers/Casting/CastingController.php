<?php

namespace App\Http\Controllers\Casting;

use App\Casting;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiController;

class CastingController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Swagger Annotations
     * * @OA\Get(
     *     path="/casting",
     *     tags={"Casting"},
     *     summary="Get casting list",
     *     description="Returns casting list.",
     *     operationId="index",
     *     @OA\Response(
     *         response=200,
     *         description="Casting overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action."
     *     )
     * )
     *
     */
    public function index()
    {
        $casting = Casting::has('movies')->get();
        return $this->showAll($casting);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Swagger Annotation
     * @OA\Get(
     *     path="/casting/{id}",
     *     tags={"Casting"},
     *     summary="Get a casting info",
     *     description="Returns a casting info.",
     *     operationId="show",
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         description="Casting ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Casting overview."
     *     ),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized action.",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Casting not found.",
     *     )
     * )
     *
     */
    public function show(Casting $casting)
    {
        return $this->showOne($casting);
    }
}
