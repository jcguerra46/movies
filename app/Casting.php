<?php

namespace App;

use App\Scopes\CastingScope;

class Casting extends User
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CastingScope);
    }

    /**
     * Relation belongsToMany with movie model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function movies()
    {
        return $this->belongsToMany('\App\Movie','casting_movie')
            ->withPivot('casting_id');
    }
}
