<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Movie extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'release_year',
        'synopsis',
        'image_id'
    ];

    /**
     * Relation belongsToMany with Director Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function directors()
    {
        return $this->belongsToMany(Director::class);
    }

    /**
     * Relation belongsToMany with Producer Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function producers()
    {
        return $this->belongsToMany(Producer::class);
    }

    /**
     * Relation belongsToMany with Casting Model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function casting()
    {
        return $this->belongsToMany(Casting::class);
    }

    /**
     * Relation belongsTo with image model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    /**
     * Accessor
     * Get the release year in roman numerals
     *
     * @param $value
     * @return string
     */
    public function getReleaseYearAttribute($value)
    {
        return $this->convertIntegerToRomanNumerals($value);
    }

    /**
     * Convert an integer to roman numerals
     *
     * @param $value
     * @return string
     */
    public function convertIntegerToRomanNumerals($value)
    {
        $n = intval($value);
        $result = '';
        $romanNumerals = [
            'M'  => 1000,
            'CM' => 900,
            'D'  => 500,
            'CD' => 400,
            'C'  => 100,
            'XC' => 90,
            'L'  => 50,
            'XL' => 40,
            'X'  => 10,
            'IX' => 9,
            'V'  => 5,
            'IV' => 4,
            'I'  => 1
        ];

        foreach ($romanNumerals as $roman => $number)
        {
            $matches = intval($n / $number);
            $result .= str_repeat($roman, $matches);
            $n = $n % $number;
        }
        return $result;
    }
}
