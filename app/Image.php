<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $uploads = '/images/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'file'
    ];

    /**
     * Accesor
     * Get path of the image
     *
     * @param $value
     * @return string
     */
    public function getFileAttribute($value)
    {
        return $this->uploads . $value;
    }
}
