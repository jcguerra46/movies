<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The database connection used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    use Notifiable, SoftDeletes;

    const USER_VERIFIED = '1';
    const USER_NOT_VERIFIED = '0';

    const ADMIN_USER = 'true';
    const REGULAR_USER = 'false';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'aliases',
        'email',
        'biography',
        'image_id',
        'password',
        'remember_token',
        'verified',
        'verification_token',
        'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'verification_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Relation belongsTo with image model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo(Image::class);
    }

    /**
     * Mutator
     * Set the first name in lowercase
     *
     * @param $value
     * @return string
     */
    public function setFirstNameAttribute($value)
    {
        return $this->attributes['first_name'] = strtolower($value);
    }

    /**
     * Accesor
     * Convert the first character of the string to uppercase
     *
     * @param $value
     * @return string
     */
    public function getFisrtNameAttribute($value)
    {
        return ucwords($value);
    }

    /**
     * Mutator
     * Set the last name in lowercase
     *
     * @param $value
     * @return string
     */
    public function setLastNameAttribute($value)
    {
        return $this->attributes['last_name'] = strtolower($value);
    }

    /**
     * Accesor
     * Convert the first character of the string to uppercase
     *
     * @param $value
     * @return string
     */
    public function getLastNameAttribute($value)
    {
        return ucwords($value);
    }

    /**
     * Mutator
     * Set the email in lowercase
     *
     * @param $value
     * @return string
     */
    public function setEmailAttribute($value)
    {
        return $this->attributes['email'] = strtolower($value);
    }

    /**
     * Mutator
     * Set bycrypt password
     *
     * @param $value
     * @return string
     */
    public function setPasswordAttribute($value)
    {
        return $this->attributes['password'] = bcrypt($value);
    }

    /**
     * User Verified
     *
     * @return string
     */
    public function isVerified()
    {
        return $this->verified = User::USER_VERIFIED;
    }

    /**
     * is Admin
     *
     * @return string
     */
    public function isAdmin()
    {
        return $this->isAdmin = User::ADMIN_USER;
    }

    /**
     * Generate token verification
     *
     * @param $number
     * @return string
     */
    public static function generateTokenVerification($number)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $number; $i++) {
            $index = rand(0, strlen($characters) - 1);
            $randomString .= $characters[$index];
        }
        return $randomString;
    }
}
