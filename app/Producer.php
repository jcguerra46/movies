<?php

namespace App;

use App\Scopes\ProducerScope;

class Producer extends User
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ProducerScope);
    }

    /**
     * Relation belongsToMany with movie model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function movies()
    {
        return $this->belongsToMany('\App\Movie','movie_producer')
            ->withPivot('producer_id');
    }
}
