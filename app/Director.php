<?php

namespace App;

use App\Scopes\DirectorScope;

class Director extends User
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new DirectorScope);
    }

    /**
     * Relation belongsToMany with movie model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function movies()
    {
        return $this->belongsToMany('\App\Movie','director_movie')
            ->withPivot('director_id');
    }
}
